# Util
#### by Karlito Web

***

### Usage:  
``` php
use KarlitoWeb\Toolbox\String\Util;
```

#### Check if a given string matches a given pattern.
``` php
Util::match(string $first, string $second)
```

#### Generates a string of random characters.
``` php
Util::ramdom(int $number, bool $human = FALSE, bool $symbol = TRUE, bool $unique = TRUE)
```

#### Pads a given string with zeroes on the left.
``` php
Util::zero_pad(int $number, int $lenght)
```

#### Convert entities, while preserving already-encoded entities.
``` php
Util::htmlentities(string $string, bool $preserve_encoded_entities = TRUE)
```

#### Converts any accent characters to their equivalent normal characters
``` php
Util::slugify(string $string, string $separator = '-')
```

#### Truncate a string to a specified length without cutting a word off.
``` php
Util::truncate(string $string, int $length, string $append = '...')
```

#### Strip all withspace from the given string.
``` php
Util::stripSpace(string $string)
```
