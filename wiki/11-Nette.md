# Nette
#### by Karlito Web

***
### Usage:  
``` php
use KarlitoWeb\Toolbox\String\Nette;
```

``` php
Nette::slugify(string $string, string $separator = '-'): string
```

``` php
Nette::truncate(string $string, int $length = 80, string $append = '...'): string
```

``` php
Nette::ramdom(int $length = 16, string $charlist = '0-9a-z'): string
```

``` php
Nette::pad(string $string, int $length, string $pad = '0'): string
```

``` php
Nette::toAscii(string $string): string
```

``` php
Nette::isEmail(string $email): bool
```

``` php
Nette::isContains(string $haystack, string $needle): bool
```
