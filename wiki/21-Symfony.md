# Symfony
#### by Karlito Web

***

### Usage:  
``` php
use KarlitoWeb\Toolbox\String\Symfony;
```

#### Returns the slug-version of the string.
``` php
Symfony::slugify($string, $separator = '-')
```

#### Reduces the string to the length given as argument (if it's longer).
``` php
Symfony::truncate($string, int $length, string $append = '...')
```

#### Replaces all occurrences of the given string.
``` php
Symfony::replace($string, $search, $replace)
```

#### Rremoves all white spaces.
``` php
Symfony::collapseWhiteSpace($string)
```

#### Uses the string as the "glue" to merge all the given strings.
``` php
Symfony::join(array $words, $glue);
```

#### Makes a string as long as the first argument by adding the given string at the beginning, end or both sides of the string.
``` php
Symfony::padBoth(int $length, $string, $pad)
```

#### Makes a string as long as the first argument by adding the given string at the beginning, end or both sides of the string.
``` php
Symfony::padStart(int $length, $string, $pad)
```

#### Makes a string as long as the first argument by adding the given string at the beginning, end or both sides of the string.
``` php
Symfony::padEnd(int $length, $string, $pad)
```

#### Changes all graphemes/code points to lower case.
``` php
Symfony::lower($string)
```

#### Changes all graphemes/code points to upper case.
``` php
Symfony::upper($string)
```

#### Changes all graphemes/code points to title case.
``` php
Symfony::title($string)
```

#### Changes all graphemes/code points to camel case.
``` php
Symfony::camel($string)
```

#### Changes all graphemes/code points to snake case.
``` php
Symfony::snake($string)
```

#### Returns the number of graphemes, code points or bytes of the given string.
``` php
Symfony::length($string)
```

#### Breaks the string into pieces of the length given as argument.
``` php
Symfony::chunk($string, int $lenght)
```

#### XXX.
``` php
Symfony::pluralizeEnglish($word)
```

#### XXX.
``` php
Symfony::singularizeEnglish($word)
```

#### XXX.
``` php
Symfony::pluralizeFrench($word)
```

#### XXX.
``` php
Symfony::singularizeFrench($word)
```
