<?php

declare(strict_types=1);

namespace KarlitoWeb\Toolbox\String;

use KarlitoWeb\Toolbox\String\Interfaces\StringInterface;
use Symfony\Component\String\Inflector\EnglishInflector;
use Symfony\Component\String\Inflector\FrenchInflector;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Symfony\Component\String\UnicodeString;

/**
 * @author      Karlito15                               <giancarlo.palumbo@free.fr>
 * @license     https://opensource.org/license/mit/     MIT
 * @link        https://doc.nette.org/
 * @package     karlito-web/toolbox-php-string
 * @subpackage  symfony/string
 * @version     3.0.0
 */
class Symfony implements StringInterface
{
    /**
     * Returns the slug-version of the string.
     *
     * @param string $string        String to slugify
     * @param string $separator     Options
     * @return string
     */
    public static function slugify(string $string, string $separator = '-'): string
    {
        $slugger = new AsciiSlugger();
        $slugger->setLocale('fr');

        return $slugger->slug($string, $separator)->lower()->toString();
    }

    /**
     * Reduces the string to the length given as argument (if it's longer)
     *
     * @param string $string
     * @param int    $length
     * @param string $append
     * @return string
     */
    public static function truncate(string $string, int $length = 80, string $append = '...'): string
    {
        return self::init($string)->truncate($length, $append)->toString();
    }

    /**
     * Rremoves all white spaces from the start and end of the string and replaces two
     * or more consecutive white spaces inside contents by a single white space
     *
     * @param string $string
     * @return string
     */
    public static function collapseWhiteSpace(string $string): string
    {
        return self::init($string)->collapseWhitespace()->toString();
    }

    /**
     * Replaces all occurrences of the given string
     *
     * @param string $string
     * @param string $search
     * @param string $replace
     * @return string
     */
    public static function replace(string $string, string $search, string $replace): string
    {
        return self::init($string)->replace($search, $replace)->toString();
    }

    /**
     * Uses the string as the "glue" to merge all the given strings
     *
     * @param array  $words
     * @param string $glue
     * @return string
     */
    public static function join(array $words, string $glue): string
    {
        return self::init($glue)->join($words)->toString();
    }

    /**
     * Makes a string as long as the first argument by adding the given string at the beginning, end or both sides of the string
     *
     * @param int    $length
     * @param string $string
     * @param string $pad
     * @return string
     */
    public static function padBoth(int $length, string $string, string $pad): string
    {
        return self::init($string)->padBoth($length, $pad)->toString();
    }

    /**
     * Makes a string as long as the first argument by adding the given string at the beginning, end or both sides of the string
     *
     * @param int    $length
     * @param string $string
     * @param string $pad
     * @return string
     */
    public static function padStart(int $length, string $string, string $pad): string
    {
        return self::init($string)->padStart($length, $pad)->toString();
    }

    /**
     * Makes a string as long as the first argument by adding the given string at the beginning, end or both sides of the string
     *
     * @param int    $length
     * @param string $string
     * @param string $pad
     * @return string
     */
    public static function padEnd(int $length, string $string, string $pad): string
    {
        return self::init($string)->padEnd($length, $pad)->toString();
    }

    /**
     * Changes all graphemes/code points to lower case
     *
     * @param string $string
     * @return string
     */
    public static function lowerCase(string $string): string
    {
        return self::init($string)->lower()->toString();
    }

    /**
     * Changes all graphemes/code points to upper case
     *
     * @param string $string
     * @return string
     */
    public static function upperCase(string $string): string
    {
        return self::init($string)->upper()->toString();
    }

    /**
     * Changes all graphemes/code points to title case
     *
     * @param string $string
     * @return string
     */
    public static function titleCase(string $string): string
    {
        return self::init($string)->title(true)->toString();
    }

    /**
     * Changes all graphemes/code points to camel case
     *
     * @param string $string
     * @return string
     */
    public static function camelCase(string $string): string
    {
        return self::init($string)->camel()->toString();
    }

    /**
     * Changes all graphemes/code points to snake case
     *
     * @param string $string
     * @return string
     */
    public static function snakeCase(string $string): string
    {
        return self::init($string)->snake()->toString();
    }

    /**
     * Returns the number of graphemes, code points or bytes of the given string
     *
     * @param string $string
     * @return int
     */
    public static function length(string $string): int
    {
        return self::init($string)->length();
    }

    /**
     * Breaks the string into pieces of the length given as argument
     * @param string $string
     * @param int    $length
     * @return array
     */
    public static function chunk(string $string, int $length): array
    {
        return self::init($string)->chunk($length);
    }

    /**
     *
     * @param string $word
     * @return array
     */
    public static function pluralizeEnglish(string $word): array
    {
        $inflector = new EnglishInflector();

        return $inflector->pluralize($word);
    }

    /**
     *
     * @param string $word
     * @return array
     */
    public static function singularizeEnglish(string $word): array
    {
        $inflector = new EnglishInflector();

        return $inflector->singularize($word);
    }

    /**
     *
     * @param string $word
     * @return array
     */
    public static function pluralizeFrench(string $word): array
    {
        $inflector = new FrenchInflector();

        return $inflector->pluralize($word);
    }

    /**
     *
     * @param string $word
     * @return array
     */
    public static function singularizeFrench(string $word): array
    {
        $inflector = new FrenchInflector();

        return $inflector->singularize($word);
    }

    /**
     *
     * @param string $string
     * @return UnicodeString
     */
    private static function init(string $string): UnicodeString
    {
        return (new UnicodeString($string));
    }
}
