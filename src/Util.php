<?php

declare(strict_types=1);

namespace KarlitoWeb\Toolbox\String;

use KarlitoWeb\Toolbox\String\Interfaces\StringInterface;
use utilphp\util as UtilPHP;

/**
 * @author      Karlito15                               <giancarlo.palumbo@free.fr>
 * @license     https://opensource.org/license/mit/     MIT
 * @link        https://doc.nette.org/
 * @package     karlito-web/toolbox-php-string
 * @subpackage  brandonwamboldt/utilphp
 * @version     3.0.0
 */
class Util implements StringInterface
{
    /**
     * Converts any accent characters to their equivalent normal characters
     * and converts any other non-alphanumeric characters to dashes, then
     * converts any sequence of two or more dashes to a single dash. This
     * function generates slugs safe for use as URLs, and if you pass true
     * as the second parameter, it will create strings safe for use as CSS
     * classes or IDs.
     *
     * @param string $string
     * @param string $separator
     * @return string
     */
    public static function slugify(string $string, string $separator = '-'): string
    {
        return UtilPHP::slugify($string, $separator);
    }

    /**
     * Truncate a string to a specified length without cutting a word off.
     *
     * @param   string  $string  The string to truncate
     * @param   integer $length  The length to truncate the string to
     * @param   string  $append  Text to append to the string IF it gets truncated, defaults to '...'
     * @return  string
     */
    public static function truncate(string $string, int $length = 80, string $append = '...'): string
    {
        return UtilPHP::safe_truncate($string, $length, $append);
    }

    /**
     * Generates a string of random characters.
     *
     * @param int  $number The length of the string to generate
     * @param bool $human  Whether to make the string human friendly by removing characters that can be confused with other characters ( O and 0, l and 1, etc)
     * @param bool $symbol Whether to include symbols in the string. Can not be enabled if $human_friendly is true
     * @param bool $unique Whether to only use characters once in the string.
     * @return string
     */
    public static function ramdom(int $number, bool $human = false, bool $symbol = true, bool $unique = true): string
    {
        return UtilPHP::random_string($number, $human, $symbol, $unique);
    }

    /**
     * Pads a given string with zeroes on the left.
     *
     * @param int $number The number to pad
     * @param int $lenght The total length of the desired string
     * @return string
     */
    public static function zero_pad(int $number, int $lenght): string
    {
        return UtilPHP::zero_pad($number, $lenght);
    }

    /**
     * Convert entities, while preserving already-encoded entities.
     *
     * @param string $string
     * @param bool   $preserve_encoded_entities
     * @return string
     */
    public static function htmlentities(string $string, bool $preserve_encoded_entities = true): string
    {
        return UtilPHP::htmlentities($string, $preserve_encoded_entities);
    }

    /**
     * Strip all withspace from the given string.
     *
     * @param string $string
     * @return string
     */
    public static function stripSpace(string $string): string
    {
        return UtilPHP::strip_space($string);
    }

    /**
     * Check if a given string matches a given pattern.
     *
     * @param string $first  Parttern of string exptected
     * @param string $second String that need to be matched
     * @param bool   $caseSensitive
     * @return bool
     */
    public static function isContains(string $first, string $second, bool $caseSensitive = true): bool
    {
        return UtilPHP::match_string($first, $second, $caseSensitive);
    }
}
