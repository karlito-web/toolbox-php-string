<?php

declare(strict_types=1);

namespace KarlitoWeb\Toolbox\String;

use KarlitoWeb\Toolbox\String\Interfaces\StringInterface;
use Nette\Utils\Random;
use Nette\Utils\Strings;
use Nette\Utils\Validators;

/**
 * @author      Karlito15                               <giancarlo.palumbo@free.fr>
 * @license     https://opensource.org/license/mit/     MIT
 * @link        https://doc.nette.org/
 * @package     karlito-web/toolbox-php-string
 * @subpackage  nette/utils
 * @version     3.0.0
 */
class Nette implements StringInterface
{
    /**
     * Returns the slug-version of the string.
     *
     * @param string    $string  String to slugify
     * @param string    $separator Options
     * @return string   Slugified version of the string
     */
    public static function slugify(string $string, string $separator = '-'): string
    {
        return Strings::webalize($string, null, true);
    }

    /**
     * Truncates a UTF-8 string to given maximal length, while trying not to split whole words.
     * Only if the string is truncated, an ellipsis (or something else set with third argument)
     * is appended to the string.
     *
     * @param string $string
     * @param int    $length
     * @param string $append
     * @return string
     */
    public static function truncate(string $string, int $length = 80, string $append = '...'): string
    {
        return Strings::truncate($string, $length, $append);
    }

    /**
     * Generates a random string of given length from characters specified in second argument.
     * Supports intervals, such as 0-9 or A-Z.
     *
     * @param int    $length
     * @param string $charlist
     * @return string
     */
    public static function ramdom(int $length = 16, string $charlist = '0-9a-z'): string
    {
        return Random::generate($length, $charlist);
    }

    /**
     * Pads a UTF-8 string to given length by prepending the $pad string to the beginning..
     *
     * @param string $string
     * @param int    $length
     * @param non-empty-string $pad
     * @return string
     */
    public static function pad(string $string, int $length, string $pad = '0'): string
    {
        return Strings::padLeft($string, $length, $pad);
    }

    /**
     * Converts UTF-8 string to ASCII, ie removes diacritics etc.
     *
     * @param string $string
     * @return string
     */
    public static function toAscii(string $string): string
    {
        return Strings::toAscii($string);
    }

    /**
     * @param string $email
     * @return bool
     */
    public static function isEmail(string $email): bool
    {
        if (Validators::isEmail($email)) {
            return true;
        }

        return false;
    }

    /**
     * Checks if $haystack string contains $needle.
     *
     * @param string $haystack
     * @param string $needle
     * @return bool
     */
    public static function isContains(string $haystack, string $needle): bool
    {
        return Strings::contains($haystack, $needle);
    }
}
