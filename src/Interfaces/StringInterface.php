<?php

declare(strict_types=1);

namespace KarlitoWeb\Toolbox\String\Interfaces;

interface StringInterface
{
    /**
     * @param string $string
     * @param string $separator
     * @return string
     */
    public static function slugify(string $string, string $separator = '-'): string;

    /**
     * @param string $string
     * @return string
     */
    public static function truncate(string $string, int $length = 80, string $append = '...'): string;
}
