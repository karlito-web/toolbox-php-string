<?php

namespace KarlitoWeb\Toolbox\String\tests;

use KarlitoWeb\Toolbox\String\Symfony;
use PHPUnit\Framework\TestCase;

class SymfonyTest extends TestCase
{
    /**
     * @param string $expected
     * @covers Symfony::slugify
     * @dataProvider loremProvider
     */
    public function testEquals(string $expected): void
    {
        $result = "lorem-ipsum-dolor-sit-amet-consectetuer-adipiscing-elit";
        $test   = Symfony::slugify($expected);
        $this->assertEquals($result, $test);
        // dump($test);

        $result = "Lorem...";
        $test   = Symfony::truncate($expected, 8);
        $this->assertEquals($result, $test);
        // dump($test);

        $test   = Symfony::collapseWhiteSpace("   \n  Lorem    ipsum dolor\nsit amet, consectetuer adipiscing elit.   \n\n  \n  ");
        $this->assertEquals($expected, $test);
        // dump($test);

        $result = '---10---';
        $test   = Symfony::padBoth(8, "10", '-');
        $this->assertEquals($result, $test);
        // dump($test);

        $result = '000010';
        $test   = Symfony::padStart(6, 10, '0');
        $this->assertEquals($result, $test);
        // dump($test);

        $result = '10AAAA';
        $test   = Symfony::padEnd(6, 10, 'A');
        $this->assertEquals($result, $test);
        // dump($test);
    }

    /**
     * @param string $expected
     * @covers Symfony::slugify
     * @dataProvider loremProvider
     */
    public function testisString(string $expected): void
    {
        $this->assertIsString(Symfony::slugify($expected));
        $this->assertIsString(Symfony::truncate($expected, 8));
        $this->assertIsString(Symfony::collapseWhiteSpace("   \n  Lorem    ipsum dolor\nsit amet, consectetuer adipiscing elit.   \n\n  \n  "));
        $this->assertIsString(Symfony::padBoth(8, "10", '-'));
        $this->assertIsString(Symfony::padStart(6, 10, '0'));
        $this->assertIsString(Symfony::padEnd(6, 10, 'A'));
    }

    /**
     * @param string $expected
     * @covers Symfony::slugify
     * @dataProvider loremProvider
     */
    public function testTrue(string $expected): void
    {
        $this->assertTrue(strlen(Symfony::slugify($expected)) == 55);
        $this->assertTrue(strlen(Symfony::truncate($expected, 8)) == 8);
        $this->assertTrue(strlen(Symfony::collapseWhiteSpace("   \n  Lorem    ipsum dolor\nsit amet, consectetuer adipiscing elit.   \n\n  \n  ")) == 57);
        $this->assertTrue(strlen(Symfony::padBoth(8, "10", '-')) == 8);
        $this->assertTrue(strlen(Symfony::padStart(6, 10, '0')) == 6);
        $this->assertTrue(strlen(Symfony::padEnd(6, 10, 'A')) == 6);
    }

    /**  @return array */
    public function loremProvider(): array
    {
        return [
            ["Lorem ipsum dolor sit amet, consectetuer adipiscing elit."],
            // ["Lorém ìpsum dôlör sit amèt, consectetùer àdipiscing elit."],
        ];
    }
}
