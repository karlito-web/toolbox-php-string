<?php

namespace KarlitoWeb\Toolbox\String\tests;

use KarlitoWeb\Toolbox\String\Util;
use PHPUnit\Framework\TestCase;

class UtilTest extends TestCase
{
    /**
     * @param string $expected
     * @covers Util::slugify
     * @dataProvider loremProvider
     */
    public function testEquals(string $expected): void
    {
        $result = "lorem-ipsum-dolor-sit-amet-consectetuer-adipiscing-elit-";
        $test   = Util::slugify($expected);
        $this->assertEquals($result, $test);

        $result = "0010";
        $test   = Util::zero_pad(10, 4);
        $this->assertEquals($result, $test);

        $result = "Lorem...";
        $test   = Util::truncate($expected, 8);
        $this->assertEquals($result, $test);

        $result = 'Une &#039;apostrophe&#039; en &lt;strong&gt;gras&lt;/strong&gt;';
        $test   = Util::htmlentities('Une \'apostrophe\' en <strong>gras</strong>');
        $this->assertEquals($result, $test);

        $result = 'Loremipsumdolorsitamet,consectetueradipiscingelit.';
        $test   = Util::stripSpace($expected);
        $this->assertEquals($result, $test);
    }

    /**
     * @param string $expected
     * @covers Util::slugify
     * @dataProvider loremProvider
     */
    public function testisString(string $expected): void
    {
        $this->assertIsString(Util::slugify($expected));
        $this->assertIsString(Util::ramdom(16, true, true, false));
        $this->assertIsString(Util::zero_pad(10, 4));
        $this->assertIsString(Util::truncate($expected, 8));
        $this->assertIsString(Util::htmlentities('Une \'apostrophe\' en <strong>gras</strong>'));
        $this->assertIsString(Util::stripSpace($expected));
    }

    /**
     * @param string $expected
     * @covers Util::slugify
     * @dataProvider loremProvider
     */
    public function testTrue(string $expected): void
    {
        $this->assertTrue(strlen(Util::slugify($expected)) == 56);
        $this->assertTrue(strlen(Util::ramdom(16, true, true, false)) == 16);
        $this->assertTrue(strlen(Util::zero_pad(10, 4)) == 4);
        $this->assertTrue(strlen(Util::truncate($expected, 8)) == 8);
        // $this->assertTrue(Util::isContains("ipsum", $expected));
    }

    /** @return array */
    public function loremProvider(): array
    {
        return [
            ["Lorem ipsum dolor sit amet, consectetuer adipiscing elit."],
            // ["Lorém ìpsum dôlör sit amèt, consectetùer àdipiscing elit."],
        ];
    }
}
