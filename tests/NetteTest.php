<?php

namespace KarlitoWeb\Toolbox\String\tests;

use PHPUnit\Framework\TestCase;
use KarlitoWeb\Toolbox\String\Nette;

class NetteTest extends TestCase
{
    /**
     * @covers Nette::truncate
     * @covers Nette::pad
     * @covers Nette::toAscii
     * @covers Nette::slugify
     */
    public function testEquals(): void
    {
        $expected   = "Lorem...";
        $test       = Nette::truncate("Lorem ipsum dolor sit amet, consectetuer adipiscing elit.", 8);
        $this->assertEquals($test, $expected);

        $expected   = '000010';
        $test       = Nette::pad(10, 6);
        $this->assertEquals($test, $expected);

        $expected   = 'eeaAEUIOaeuioAEUIOaeuioao';
        $test       = Nette::toAscii("éèàÄËÜÏÖäëüïöÂÊÛÎÔâêûîôãõ");
        $this->assertEquals($test, $expected);

        $expected   = "lorem-ipsum-dolor-sit-amet-consectetuer-adipiscing-elit";
        $test       = Nette::slugify("Lorem ipsum dolor sit amet, consectetuer adipiscing elit.");
        $this->assertEquals($test, $expected);
    }

    /**
     * @covers Nette::slugify
     * @covers Nette::ramdom
     * @covers Nette::truncate
     * @covers Nette::pad
     * @covers Nette::toAscii
     */
    public function testisString(): void
    {
        $this->assertIsString(Nette::slugify("Lorem ipsum dolor sit amet, consectetuer adipiscing elit."));
        $this->assertIsString(Nette::ramdom(12));
        $this->assertIsString(Nette::truncate("Lorem ipsum dolor sit amet, consectetuer adipiscing elit.", 16));
        $this->assertIsString(Nette::pad(10, 4, 0));
        $this->assertIsString(Nette::toAscii("Lorem ipsum dolor sit amet, consectetuer adipiscing elit."));
    }

    /**
     * @covers Nette::ramdom
     * @covers Nette::pad
     * @covers Nette::isEmail
     * @covers Nette::isContains
     * @dataProvider loremProvider
     */
    public function testTrue(string $expected): void
    {
        $this->assertTrue(strlen(Nette::ramdom(12)) == 12);
        $this->assertTrue(strlen(Nette::pad(10, 6)) == 6);
        $this->assertTrue(Nette::isEmail('giancarlo.palumbo@free.fr'));
        $this->assertTrue(Nette::isContains($expected, "ipsum"));
    }

    /**  @return array */
    public function loremProvider(): array
    {
        return [
            ["Lorem ipsum dolor sit amet, consectetuer adipiscing elit."],
            // ["Lorém ìpsum dôlör sit amèt, consectetùer àdipiscing elit."],
        ];
    }
}
